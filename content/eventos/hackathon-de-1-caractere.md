---
title: "Hackathon de 1 Caractere"
date: 2022-11-09
draft: false
categories: ["Eventos"]
authors: ["Pedro Sader Azevedo"]
dataDoEvento: ["10","11","2022"]
---

![](/imgs/eventos/hackathon-de-1-caractere/banner.png)

Nessa quinta-feira, o LKCAMP oferecerá um workshop na Semana da Computação da Unicamp (SECOMP) onde você aprenderá a fazer sua primeira contribuição *open source*!

O título do workshop é "Hackathon de 1 Caractere", pois a contribuição que escolhemos pode ser feita com a adição de um único caractere! Mas não se deixe enganar pela simplicidade, esse caractere é especial e adiciona um comportamento interessante ao programa que ele for adicionado. :eyes:

Além disso, ensinaremos os fundamentos de colaboração em software (`git`, `bash`, GitHub, GitLab, etc) então não perca essa oportunidade!

O evento ocorrerá das 8h00 ao 12h00 na sala IC-303 do IC3, com direito à intervalo com *coffee break*! Nos vemos lá! 🐧
