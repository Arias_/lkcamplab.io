---
title: "Hackathon de 1 Caractere - Parte II"
date: 2022-11-09
draft: false
categories: ["Tutorial"]
tags: ["Hackathon", "GTK", "SECOMP"]
authors: ["Pedro Sader Azevedo"]
---

Neste evento, ensinaremos você a fazer sua primeira contribuição *open-source*! Para isso, escolhemos o GNOME, um dos mais importantes projetos no mundo do desktop GNU/Linux.

## Setup do ambiente de desenvolvimento

Vamos começar pela configuração do ambiente de desenvolvimento, assim você pode ler o restante do tutorial enquanto aguarda a instalação dos programas!

### Computador do IC

Caso você esteja um computador do IC, faça login com as credenciais:

> usuário: `aluno`
>
> senha: `secomp2022builder`

Já adiantamos a maior parte da configuração do ambiente desenvolvimento nesse usuário! Para completar o *setup*, basta abrir o atalho nomeado "Install Builder". Isso executará um script que instalará pacotes adicionais necessários.

### Computador pessoal

Caso você tenha trazido o próprio computador é necessário seguir os passos abaixo:

1. Habilitar o suporte a flatpaks

Flatpak é um formato de empacotamento de aplicações gráficas, que funciona em qualquer distribuição de GNU/Linux (Fedora, Ubuntu, Arch, etc). Esse é o formato escolhido pelo Projeto GNOME para publicar seus pacotes, então o usaremos para instalar os aplicativos e suas dependências.

- **Fedora**: Já vem instalado

- **Ubuntu**:

```bash
  sudo apt install flatpak
```

- **Arch**:

```bash
  sudo pacman -S flatpak
```

2. Adicionar os catálogos do Flathub e do GNOME Nightly

O flatpak permite o uso de diversos catálogos de aplicativos. Vamos adicionar o catálogo principal (Flathub) e o catálogo de aplicativos do GNOME que estão em fase de testes (GNOME Nightly).


```bash
flatpak remote-add --if-not-exists flathub \
        https://flathub.org/repo/flathub.flatpakrepo

flatpak remote-add --if-not-exists gnome-nightly \
        https://nightly.gnome.org/gnome-nightly.flatpakrepo
```

3. Instalar o GNOME Builder

Por fim, vamos instalar o GNOME Builder, o IDE criado especificamente para desenvolver software para o Projeto GNOME.

```bash
flatpak install org.gnome.Builder
```

4. Compilar o aplicativo escolhido

Se você já escolheu um aplicativo desta [planilha](https://ethercalc.net/s16y119kb4z9), obtenha uma cópia local usando o `git clone` (como você aprendeu na Parte I) ou usando a funcionalidade equivalente do próprio GNOME Builder. Em seguida, inicie a compilação do programa clicando no ícone de martelo na barra superior ou usando o atalho `Ctrl` + `Alt` + `Shift` + `B`. Com isso, o Builder vai baixar e instalar as dependências de compilação automaticamente.

## O que é esse tal de GNOME?

Como dito na introdução, o GNOME é um dos mais importantes projetos *open source* para o uso do GNU/Linux em computadores pessoais.

O objetivo do GNOME é se consolidar como uma plataforma de software para o desktop Linux, assim como o Android se consolidou como uma plataforma de software de para o smartphone Linux.

Isso significa padronizar a experiência de seus usuários e desenvolvedores, com um ecossistema consistente de aplicações, bibliotecas, ferramentas de desenvolvimento, documentação, etc. Por mais ambicioso que seja esse objetivo, o Projeto GNOME já progrediu muito em fazê-lo acontecer!

![](/imgs/posts/hackathon-de-1-caractere/gnome.png)

O software mais conhecido do Projeto GNOME é seu ambiente gráfico (ou *desktop environment*), o GNOME Shell. O ambiente gráfico é o programa que faz a "linha de frente" da interação com o usuário, abarcando funcionalidades como:

- gerenciamento de janelas (maximização, minimização, etc)
- interações com o sistema (volume, brilho, logout, desligar, etc)
- exibição de contexto (notificações, horário, bateria, etc)
- lançamento de programas (menu de programas, barra de tarefas, etc)
- multitarefa (Alt-tab, áreas de trabalho, etc)

O GNOME Shell é o ambiente gráfico mais utilizado em desktops GNU/Linux (se você está usando um computador do IC, provavelmente está usando o GNOME Shell!) e, para muitos, ele é "a cara do Linux".

Além do ambiente gráfico, o Projeto GNOME mantém uma série de bibliotecas que formam as bases do funcionamento do seu ecossistema. Dessas bibliotecas, a principal é o GTK, um *framework* de desenvolvimento de aplicações gráficas para GNU/Linux.

Utilizando o GTK, o GNOME desenvolve inúmeros aplicativos. Os aplicativos-padrão de seu ambiente gráfico (calculadora, calendário, galeria, configurações, navegador de arquivos, terminal, player de vídeo, visualizador de imagens, etc) usam o GTK, assim como o próprio GNOME Builder que usaremos hoje!

Outra biblioteca crucial para o desenvolvimento de aplicativos é a libadwaita, que padroniza as interfaces geradas pelo GTK no estilo preferido pelo Projeto GNOME.

## GTK atrás das cortinas

No GTK as definições de comportamento e aparência da aplicação são feitas separadamente. O comportamento é definido com linguagens de programação como Python (.py), Rust (.rs), C (.c), Vala (.vala), ou Javascript (.js) enquanto a aparência é definida com XML (.xml) ou Blueprint (.blp).

![](/imgs/posts/hackathon-de-1-caractere/gtk.png)

Os componentes gráficos da aplicação são declarados utilizando um sistema de *widgets*, que são elementos gráficos genéricos (`GtkHeaderBar`, `GtkMenuButton`, `GtkButton`, etc). Esses *widgets* seguem o paradigma da orientação a objetos, então é muito importante ler a [documentação do GTK](https://docs.gtk.org/gtk4/) e [documentação da libadwaita](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/index.html) para compreender as propriedades e os métodos herdados e implementados por cada *widget*.

## A contribuição de 1 caractere

### Mnemônicos ou "teclas de acesso"

O GTK tem uma funcionalidade muito interessante mnememônicos, também chamados de "teclas de acesso". Ela funciona assim:

![](/imgs/posts/hackathon-de-1-caractere/mnemonics.gif)

- Ao segurar a tecla `Alt`, alguns caracteres são destacados
- Se um caractere destacado é pressionado no atalho `Alt` + `[char]`
- A ação associada ao *widget* do caractere é disparada

Para implementar um mnememônico é necessário adicionar um *underline* ("_") antes do caractere que você quer que seja destacado quando o usuário segurar a tecla `Alt`.

Além disso, é preciso configurar o *widget* para interpretar o *underline* como indicador de mnememônico. Para isso, basta atribuir o valor `True` a propriedade `use-underline` do *widget*.

Isso pode ser feito nos arquivos que configuram o comportamento ou a aparência da aplicação, dependendo da estrutura do seu projeto. Para o caso dos arquivos de comportamento, alguns exemplos constam na [documentação oficial](https://developer.gnome.org/documentation/tutorials/beginners/components/label.html?highlight=mnemonics) do Projeto GNOME. Em todo caso, nós fizemos alguns exemplos adicionais desse tipo de contribuição para servirem de consulta para vocês!

#### Exemplo em Javascript

Contribuição de Pedro Sader Azevedo para a extensão "Space Bar" ([link](https://github.com/christopher-l/space-bar/pull/3/commits/6527e5942de5997e573f8bf9ae5cf976c89415df)).

![](/imgs/posts/hackathon-de-1-caractere/mnemonics-javascript.png)

#### Exemplo em Python

Contribuição de Tárik Sá para o aplicativo "What IP" ([link](https://gitlab.gnome.org/GabMus/whatip/-/merge_requests/35/diffs)).

![](/imgs/posts/hackathon-de-1-caractere/mnemonics-python.png)

#### Exemplo em XML

Contribuição de Gustavo Pechta para o aplicativo "Komikku" ([link](https://gitlab.com/valos/Komikku/-/merge_requests/180/diffs#83c420f8542fe5bed57cdd7f5ac8234e05dce352)).

![](/imgs/posts/hackathon-de-1-caractere/mnemonics-xml.png)

### *GNOME Human Interface Guidelines*

Observe no exemplo em XML acima que o caractere destacado não necessariamente precisa ser o primeiro caractere da palavra (`A_dvanced`). Com isso em mente, podem surgir perguntas como: Qual caractere devo escolher para destacar? E a qual *widget* devo adicionar o mnemônico?

Para responder a essas perguntas, devemos recorrer às [*GNOME Human Interface Guidelines*](https://developer.gnome.org/hig/). Esse documento detalha as diretrizes do Projeto GNOME para suas interfaces gráficas, e tem uma [seção sobre os mnemônicos](https://developer.gnome.org/hig/guidelines/keyboard.html?highlight=mnemonic)! Nessa seção é orientado que:

- Se possível, destaque a primeira letra do texto do *widget*
- Se possível, escolha atalhos que podem ser feitos com uma mão só
- Evite destacar caracteres finos (como `l` e `I`)
- Evite destacar caracteres com descensores (`g`, `y`, `p`,`q`)
- Priorize *widgets* com os quais o usuário interage com mais frequência (ex: botões que são pressionados frequentemente)

Dica: o *widgets* utilizados para alternar entre diferentes contextos da aplicação, tipicamente localizado na barra superior ou na barra inferior da aplicação, é um bom candidato para receber mnemônicos!

![](/imgs/posts/hackathon-de-1-caractere/view-switcher-top.png)

![](/imgs/posts/hackathon-de-1-caractere/view-switcher-bottom.png)

### Passo a passo da contribuição

Enfim, com tudo em mãos, vocês podem começar suas contribuições!

1. Escolha um projeto da [planilha](https://ethercalc.net/s16y119kb4z9) para contribuir e coloque seu nome na coluna "Grupo".

2. Visite o link na coluna "Repositório" e faça um *fork* do projeto escolhido.

3. Clone seu *fork* em uma pasta local, assim como fez na Parte I.

4. Abra a pasta no GNOME Builder e compile o programa.

5. Encontre lugares onde faltam mnemônicos.

5. Insira *underline* nos locais apropriados e ative a propriedade `use-underline`. Use os exemplos como referência!

6. Faça *commit* das suas alterações e empurre-as para seu *fork*.

7. Faça um Merge Request (GitLab) ou Pull Request (GitHub) para o projeto.

Quando terminar sua contribuição, você pode ajudar os demais participantes ou iniciar outra contribuição!

Obrigado pela participação! Até a próxima! :rocket:
